﻿using System;
using System.Collections.Generic;

namespace Module3_2
{
    public static class Program
    {
        static void Main(string[] args)
        {
            Task8 task8 = new Task8();
            task8.FillArrayInSpiral(16);
        }
    }

    public class Task4
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {

            bool isParsed = int.TryParse(input, out result);
            if (result > 0 && isParsed)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public int[] GetFibonacciSequence(int n)
        {
            int[] arr = new int[n];
            int n0 = 1;
            int n1 = 1;
            int n2;
            if (n > 0 && n == 1)
            {
                arr[n] = 1;
                return arr;
            }
            else if (n > 0 && n == 2)
            {
                arr[0] = n0;
                arr[1] = n1;
                return arr;
            }
            else
            {
                for (int i = 3; i <= n; i++)
                {
                    n2 = n0 + n1;
                    n0 = n1;
                    n1 = n2;
                    arr[i] = n2;
                }
                return arr;
            }
        }
    }

    public class Task5
    {
        public int ReverseNumber(int sourceNumber)
        {
            int reversed = 0;
            while (sourceNumber > 10)
            {
                int number = sourceNumber % 10;
                reversed = reversed * 10 + number;
                sourceNumber /= 10;
            }

            return reversed;
        }
    }

    public class Task6
    {
        /// <summary>
        /// Use this method to generate array. It shouldn't throws exception.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int[] GenerateArray(int size)
        {
            int[] arr = new int[size];
            for (int i = 0; i < size; i++)
            {
                arr[i] = i;
            }
            return arr;
        }

        public int[] UpdateElementToOppositeSign(int[] source)
        {
            for (int i = 0; i < source.Length; i++)
            {
                if (source[i] < 0)
                {
                    source[i] = -source[i];
                }
                else
                {
                    source[i] /= 1;
                }
            }
            return source;
        }
    }

    public class Task7
    {
        /// <summary>
        /// Use this method to generate array. It shouldn't throws exception.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int[] GenerateArray(int size)
        {
            int[] arr = new int[size];
            for (int i = 0; i < size; i++)
            {
                arr[i] = i;
            }
            return arr;
        }
        public List<int> FindElementGreaterThenPrevious(int[] source)
        {
            List<int> arr = new List<int>();
            for (int i = 1; i < source.Length; i++)
            {
                if (source[i] > source[i - 1])
                {
                    arr.Add(source[i]);
                }
            }
            return arr;
        }
    }

    public class Task8
    {
        public int[,] FillArrayInSpiral(int size)
        {
            int n = size;
            int[,] matrix = new int[n, n];
            int row = 0;
            int col = 0;
            string direction = "right";
            int maxRotations = n * n;
            for (int i = 1; i <= maxRotations; i++)
            {
                if ((direction == "right") && (col > n - 1 || matrix[row, col] != 0))
                {
                    direction = "down";
                    col--;
                    row++;
                }
                if ((direction == "down") && (row > n - 1 || matrix[row, col] != 0))
                {
                    direction = "left";
                    row--;
                    col--;
                }
                if ((direction == "left") && (col < 0 || matrix[row, col] != 0))
                {
                    direction = "up";
                    col++;
                    row--;
                }
                if ((direction == "up") && (row < 0 || matrix[row, col] != 0))
                {
                    direction = "right";
                    row++;
                    col++;
                }
                matrix[row, col] = i;
                if (direction == "right")
                {
                    col++;
                }
                if (direction == "down")
                {
                    row++;
                }
                if (direction == "left")
                {
                    col--;
                }
                if (direction == "up")
                {
                    row--;
                }
            }
            return matrix;
        }
    }
}
